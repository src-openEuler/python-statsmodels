%global _empty_manifest_terminate_build 0
Name:		python-statsmodels
Version:	0.14.2
Release:	1
Summary:	Statistical computations and models for Python
License:	BSD-3-Clause
URL:		https://github.com/statsmodels/statsmodels
Source0:	%{pypi_source statsmodels}

%description
Statsmodels is a Python package that provides a complement to scipy for
statistical computations including descriptive statistics and estimation
and inference for statistical models.

%package -n python3-statsmodels
Summary:	Statistical computations and models for Python
Provides:	python-statsmodels = %{version}-%{release}
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-setuptools_scm
BuildRequires:	python3-cffi
BuildRequires:	python3-numpy
BuildRequires:	gcc
BuildRequires:	gdb
%description -n python3-statsmodels
Statsmodels for python3.

%package help
Summary:	Development documents and examples for statsmodels
Provides:	python3-statsmodels-doc
%description help
Development documents and examples for statsmodels.

%prep
%autosetup -p1 -n statsmodels-%{version}

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-statsmodels -f filelist.lst
%dir %{python3_sitearch}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Thu Aug 22 2024 Ge Wang <wang__ge@126.com> - 0.14.2-1
- Update package to version 0.14.2

* Tue Aug 29 2023 caodongxia <caodongxia@h-partners.com> - 0.14.0-2
- Add buildRequire python3-setuptools_scm

* Fri Jun 16 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 0.14.0-1
- Update package to version 0.14.0

* Fri Nov 18 2022 jiangxinyu <jiangxinyu@kylinos.cn> - 0.13.5-1
- Update package to version 0.13.5

* Fri Jul 08 2022 Xu Jin <jinxu@kylinos.cn> - 0.13.2-1
- Package Spec generated
